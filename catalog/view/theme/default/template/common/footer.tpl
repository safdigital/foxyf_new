<footer>
  <div class="container">
    <div class="row">
     
      <div class="col-sm-6">
      
        <ul class="list-unstyled menuf">
          <?php foreach ($categories as $category) { ?>
       <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        </ul>
      </div>
   
     
      <div class="col-sm-6">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="/"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
		    <a href="tel:+7 (732) 823 32 88" class="phonefooter">+7 (732) 823 32 88</a>
		  <a href="mailtop:order@foxyf.ru" class="mailfooter">order@foxyf.ru</a>
		
		<div class="social">
		<a href="" class="iconvk"></a>
		<a href=""  class="iconfb"></a>
		<a href=""  class="iconut"></a>
		</div>
        </div>
        
      </div>
    </div>
   
  </div>
</footer>



</body></html>