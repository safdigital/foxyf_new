
<div class="row imgcategory">
    <?php foreach ($categories as $category) { ?>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="product-thumb transition cathome">
              
                <h4><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h4>
            </div>
        </div>
    <?php } ?>
</div>