<?php
// HTTP
define('HTTP_SERVER', 'http://foxyf/admin/');
define('HTTP_CATALOG', 'http://foxyf/');

// HTTPS
define('HTTPS_SERVER', 'http://foxyf/admin/');
define('HTTPS_CATALOG', 'http://foxyf/');

// DIR
define('DIR_APPLICATION', 'D:/Server/OSPanel/domains/foxyf/admin/');
define('DIR_SYSTEM', 'D:/Server/OSPanel/domains/foxyf/system/');
define('DIR_IMAGE', 'D:/Server/OSPanel/domains/foxyf/image/');
define('DIR_LANGUAGE', 'D:/Server/OSPanel/domains/foxyf/admin/language/');
define('DIR_TEMPLATE', 'D:/Server/OSPanel/domains/foxyf/admin/view/template/');
define('DIR_CONFIG', 'D:/Server/OSPanel/domains/foxyf/system/config/');
define('DIR_CACHE', 'D:/Server/OSPanel/domains/foxyf/system/storage/cache/');
define('DIR_DOWNLOAD', 'D:/Server/OSPanel/domains/foxyf/system/storage/download/');
define('DIR_LOGS', 'D:/Server/OSPanel/domains/foxyf/system/storage/logs/');
define('DIR_MODIFICATION', 'D:/Server/OSPanel/domains/foxyf/system/storage/modification/');
define('DIR_UPLOAD', 'D:/Server/OSPanel/domains/foxyf/system/storage/upload/');
define('DIR_CATALOG', 'D:/Server/OSPanel/domains/foxyf/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'foxyf');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
